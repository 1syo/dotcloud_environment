require 'spec_helper'

describe DotcloudEnvironment do
  describe ".path" do
    subject { DotcloudEnvironment }
    it { subject.path.should eq '/home/dotcloud/environment.yml' }
  end

  describe ".raw" do
    context "return blank hash" do
      subject { DotcloudEnvironment.raw }
      it { subject.should eq Hash.new }
    end

    context do
      let(:path) { File.join(File.dirname(__FILE__), '..', 'support', 'environment.yml') }
      let(:env) { YAML.load_file(path) }

      before do
        DotcloudEnvironment.should_receive(:path) { path }
        DotcloudEnvironment.raw
      end

      subject { DotcloudEnvironment.raw }
      it { subject.should eq env }
    end
  end

  describe ".env" do
    let(:path) { File.join(File.dirname(__FILE__), '..', 'support', 'environment.yml') }
    let(:env) { YAML.load_file(path) }

    before { DotcloudEnvironment.stub(:path) { path } }
    subject { DotcloudEnvironment.env }

    it { subject["DOTCLOUD_DB_SQL_HOST"].should eq env["DOTCLOUD_DB_SQL_HOST"] }
    it { subject["PORT_HTTP"].should eq env["PORT_HTTP"].to_s }
  end

  describe "ENV.update not raise error" do
    let(:path) { File.join(File.dirname(__FILE__), '..', 'support', 'environment.yml') }
    let(:env) { YAML.load_file(path) }
    before { DotcloudEnvironment.stub(:path) { path } }

    it { expect{ ENV.update(DotcloudEnvironment.env) }.to_not raise_error }
  end
end
