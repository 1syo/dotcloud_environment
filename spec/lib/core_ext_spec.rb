require 'spec_helper'

describe Hash do
  describe "#stringify_values" do
    let(:hash) { { integer: 1, string: "a", array: [1, 2, 3], hash: {key: :val} } }

    subject{ hash.stringify_values }
    it { subject[:integer].should be_kind_of String }
    it { subject[:integer].should eq hash[:integer].to_s }
    it { subject[:array].should be_kind_of String }
    it { subject[:array].should eq hash[:array].to_s }
    it { subject[:hash].should be_kind_of String }
    it { subject[:hash].should eq hash[:hash].to_s }
  end
end
