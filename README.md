# DotcloudEnvironment

[![Build Status](https://secure.travis-ci.org/1syo/dotcloud_environment.png)](http://travis-ci.org/1syo/dotcloud_environment)

dotcloud_environment parses environment.yaml and updates ENV.

## Installation

Add this line to your application's Gemfile:

    gem 'dotcloud_environment'

And then execute:

    $ bundle

## Usage

Write your Gemfile

    group :production do
      gem 'dotcloud_environment', :git => 'git://github.com/1syo/dotcloud_environment.git'
    end

And write your config/database.yml

    production:
      database: dbname_production
      host: <%= ENV['DOTCLOUD_DB_SQL_HOST'] %>
      username: <%= ENV['DOTCLOUD_DB_SQL_LOGIN'] %>
      password: <%= ENV['DOTCLOUD_DB_SQL_PASSWORD'] %>
      port: <%= ENV['DOTCLOUD_DB_SQL_PORT'] %>

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
