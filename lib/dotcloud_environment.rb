require "dotcloud_environment/version"
require "dotcloud_environment/core_ext"

begin
  require "rails"
rescue LoadError
end

if defined? Rails
  require "dotcloud_environment/railtie"
end

module DotcloudEnvironment
  extend self

  def env
    raw.stringify_values
  end

  def raw
    @_yaml ||= YAML.load_file(path)
  rescue Errno::ENOENT
    {}
  end

  def path
    '/home/dotcloud/environment.yml'
  end
end
