class Hash
  def stringify_values
    self.keys.inject({}){|hash, key| hash[key] = self[key].to_s; hash }
  end
end
