module DotcloudEnvironment
  class Railtie < ::Rails::Railtie
    config.before_configuration do
      ENV.update(DotcloudEnvironment.env)
    end
  end
end
