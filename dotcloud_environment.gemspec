# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'dotcloud_environment/version'

Gem::Specification.new do |gem|
  gem.name          = "dotcloud_environment"
  gem.version       = DotcloudEnvironment::VERSION
  gem.authors       = ["TAKAHASHI Kazunari"]
  gem.email         = ["takahashi@1syo.net"]
  gem.description   = %q{dotcloud_environment parses environment.yaml and updates ENV.}
  gem.summary       = %q{dotcloud_environment parses environment.yaml and updates ENV.}
  gem.homepage      = "https://github.com/1syo/dotcloud_environment"

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.licenses = ['MIT']

  gem.add_development_dependency 'bundler', ['>= 1.0.0']
  gem.add_development_dependency 'rake', ['>= 0']
  gem.add_development_dependency 'rdoc', ['>= 0']
  gem.add_development_dependency 'rspec', ['>= 0']
end
